<?php
    require 'oper.php';
?>

<!DOCTYPE html>
<html>
    <head>
        <?php
            head();
        ?>

        <title>Servicios</title>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <?php
                    info();
                ?>
            </div>
            <!-- /.container -->
        </nav>

        <!-- Page Content -->
        <div class="container">

            <!-- Page Heading/Breadcrumbs -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Servicios
                        <small>Todo lo que necesitas en un solo lugar</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="/Peluditos.com">Inicio</a>
                        </li>
                        <li class="active">Servicios</li>
                    </ol>
                </div>
            </div>
            <!-- /.row -->

            <!-- Image Header -->
            <div class="row">
                <div class="col-lg-12">
                    <img src="img/textoPeluditos.PNG" width = "1090" height="250" alt="" />
                </div>
            </div>
            <!-- /.row -->

            <!-- Service Panels -->
            <!-- The circle icons use Font Awesome's stacked icon classes. For more information, visit http://fontawesome.io/examples/ -->
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="page-header">Panel de Servicios</h2>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="panel panel-default text-center">
                        <div class="panel-heading">
                            <span class="fa-stack fa-5x">
                                  <!--<i class="fa fa-circle fa-stack-2x text-primary"></i>
                                  <i class="fa fa-tree fa-stack-1x fa-inverse"></i>-->
                                  <img src="img/peluditos.jpg" width = "140" height="130" alt="" /></a>
                            </span>
                        </div>
                        <div class="panel-body">
                            <h4>Adopción</h4>
                            <p>Espacio para poder poner en adopción algún Peludito o si tienes uno que quieres poner en adopción aquí lo podrás hacer.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="panel panel-default text-center">
                        <div class="panel-heading">
                            <span class="fa-stack fa-5x">
                                  <!--<i class="fa fa-circle fa-stack-2x text-primary"></i>
                                  <i class="fa fa-car fa-stack-1x fa-inverse"></i>-->
                                  <img src="img/p2.jpg" width = "150" height="130" alt="" /></a>
                            </span>
                        </div>
                        <div class="panel-body">
                            <h4>Reportes</h4>
                            <p>Te brindaremos información acerca de Peluditos que necesiten de ayuda, ya se que estén extraviados, heridos etc, además si le sucede algo a tu Peludito aquí también lo podrás reportar </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="panel panel-default text-center">
                        <div class="panel-heading">
                            <span class="fa-stack fa-5x">
                                  <!--<i class="fa fa-circle fa-stack-2x text-primary"></i>
                                  <i class="fa fa-support fa-stack-1x fa-inverse"></i>-->
                                  <img src="img/p3.jpg" width = "150" height="100" alt="" /></a>
                            </span>
                        </div>
                        <div class="panel-body">
                            <h4>Eventos</h4>
                            <p>Espacio donde podrás registrar un evento que quieras hacer, o también podrás ver los eventos que se estarán promocionando para que asistas.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="panel panel-default text-center">
                        <div class="panel-heading">
                            <span class="fa-stack fa-5x">
                                  <!--<i class="fa fa-circle fa-stack-2x text-primary"></i>
                                  <i class="fa fa-shopping-cart fa-stack-1x fa-inverse"></i>-->
                                  <img src="img/p4.jpg" width = "150" height="130" alt="" /></a>
                            </span>
                        </div>
                        <div class="panel-body">
                            <h4>Testimonio</h4>
                            <p>Podrás ver los testimonios de nuestros amigos que adoptaron o los Peluditos que fueron ayudados gracias a nuestra plataforma.</p>
                        </div>
                    </div>
                </div>
                <!--<div class="col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4><i class="fa fa-fw fa-gift"></i> ADOPCIÓN</h4>
                        </div>
                        <div class="panel-body">
                            <div class="fl_left"><p align="center"><img src="img/peluditos.jpg" width = "290" height="200" alt="" /></a></div>
                            <p>Podrás ver los testimonios de nuestros amigos que adoptaron o los Peluditos que fueron ayudados gracias a nuestra plataforma.</p>
                        </div>
                    </div>
                </div>-->
            </div>

            
            <!-- /.row -->

            <hr>

            <!-- Footer -->
            <footer>
                <div class="row">
                    <div class="col-lg-12">
                        <p>&copy; Peluditos 2015</p>
                    </div>
                </div>
            </footer>

        </div>
        <!-- /.container -->

        <!-- jQuery Version 1.11.0 -->
        <script src="js/jquery-1.11.0.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>
