<?php
    require_once 'conexion.php';
    require 'oper.php';
?>

<!DOCTYPE html>
<html>
    <head>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <?php
            head();

            if($_GET['id'] == NULL)
            {
                header('Location: /Peluditos.com');
            }

            else
            {
                $consulta = mysqli_query($conexion, "SELECT Mascota FROM Adopcion WHERE Id_Adopcion = ".$_GET['id'])
                    or die ("Fallo en la consulta");
                $fila = mysqli_fetch_array($consulta);
                $nfilas = mysqli_num_rows($consulta);

                if($nfilas > 0)
                {
                    echo "<title>".$fila['Mascota']."</title>";
                }

                else
                {
                    header('Location: /Peluditos.com');
                }
            }
        ?>
    </head>

    <body>

        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <?php
                    info();
                ?>
            </div>
            <!-- /.container -->
        </nav>

        <!-- Page Content -->
        <div class="container">
            <!-- Page Heading/Breadcrumbs -->
            <div class="row">
                <div class="col-lg-12">
                    <?php
                        if($_GET['id'] == NULL)
                        {
                            header('Location: /Peluditos.com');
                        }

                        else
                        {
                            $consulta = mysqli_query($conexion, "SELECT Mascota FROM Adopcion WHERE Id_Adopcion = ".$_GET['id'])
                                or die ("Fallo en la consulta");
                            $fila = mysqli_fetch_array($consulta);

                            echo "<h1 class='page-header'>
                                    <small>
                                        Adopta a
                                    </small>
                                    ".$fila['Mascota']."
                                </h1>

                                <ol class='breadcrumb'>
                                    <li><a href='/Peluditos.com'>Inicio</a>
                                    </li>
                                    <li><a href='Adopciones.php'>Mascotas</a>
                                    </li>
                                    <li class='active'>".$fila['Mascota']."</li>
                                </ol>";
                        }
                    ?>
                </div>
            </div>
            <!-- /.row -->

            <!-- Intro Content -->
            <div class="row">
                <div class="col-md-6">
                <?php
                    $consulta = mysqli_query($conexion, 'SELECT *
                                                         FROM Adopcion as a, Ubicacion as u
                                                         WHERE Id_Adopcion = '.$_GET['id'].' and a.Id_Ubicacion = u.Id_Ubicacion')
                                or die ('Fallo en la consulta');

                    $fila = mysqli_fetch_array($consulta);
                    $fecha = substr($fila['Fecha'], 8).'/'.substr($fila['Fecha'], 5, 2).'/'.substr($fila['Fecha'], 0, 4);

                    echo "      <div class='thumbnail'>";

                    if($fila['Foto'] == "")
                    {
                        echo            "<img class='img-responsive' src='img/sinFoto.png' alt='' style='width:400px; height:350px; border:1 solid #122235'>";
                    }

                    else
                    {
                        echo            "<img class='img-responsive' src='images/$fila[Foto]' alt='' style='width:400px; height:350px; border:1 solid #122235'>";
                    }


                    echo        "</div>
                            </div>
                            <div class='col-md-6'>
                                <h1>Acerca de ".$fila['Mascota']."</h1>
                                <h3>Datos de Contacto</h3>
                                <p><b>Dueño:</b>       ".$fila['Usuario']."</p>
                                <p><b>E-mail:</b>       ".$fila['Email']."</p>
                                <p><b>Teléfono:</b>        ".$fila['Telefono']."</p>
                                <h3>Datos de la Mascota</h3>
                                <p><b>Tipo:</b>        ".$fila['Tipo']."</p>
                                <p><b>Raza:</b>        ".$fila['Raza']."</p>
                                <p><b>Color:</b>       ".$fila['Color']."</p>
                                <p><b>Género:</b>      ".$fila['Genero']."</p>
                                <p><b>Descripción:</b>     ".$fila['Descripcion']."</p>
                                <p><b>Ubicación:</b>     ".$fila['Estado']."</p>
                                <br>
                                <p><b>Fecha de publicación:</b> ".$fecha."</p>";

                    if($fila['Status'] == 'Activo')
                    {
                        echo "  <br>
                                <p><b>Status: <font color='#088A08'>".$fila['Status']."</font></b></p>";
                    }

                    elseif ($fila['Status'] == 'Concluído')
                    {
                        echo "  <br>
                                <p><b>Status: <font color='#DF7401'>".$fila['Status']."</font></b></p>";
                    }

                    elseif ($fila['Status'] == 'Expirado')
                    {
                        echo "  <br>
                                <p><b>Status: <font color='#DF0101'>".$fila['Status']."</font></b></p>";
                    }
                ?>
                    
                        <a href="Adopciones.php">
                            <input type='submit' class="btn btn-primary" name='regresar' id='regresar' value='Regresar'/>
                        </a>
                    </div>
                </div>
            </div>
            <!-- /.row -->

           
            <!-- /.row -->


            <hr>

            <!-- Footer -->
            <footer>
                <div class="row">
                    <div class="col-lg-12">
                        <p>&copy; Peluditos 2015</p>
                    </div>
                </div>
            </footer>

        </div>
        <!-- /.container -->

        <!-- jQuery Version 1.11.0 -->
        <script src="js/jquery-1.11.0.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>
