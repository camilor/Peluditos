<?php
    session_start();
    require_once 'conexion.php';
    require 'oper.php';
?>

<!DOCTYPE html>
<html>
    <head>
        <?php
            head();
        ?>

        <title>Peluditos</title>
    </head>

    <body>
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <?php
                    info();
                    error();
                ?>
            </div>
            <!-- /.container -->
        </nav>

        <!-- Header Carousel -->
        <header id="myCarousel" class="carousel slide">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
                <li data-target="#myCarousel" data-slide-to="3"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active">
                    <div class="fill" style="background-image:url('../img/p2.jpg');"></div>
                    <div class="carousel-caption">
                        <h2>Reportes</h2>
                    </div>
                </div>
                <div class="item">
                    <div class="fill" style="background-image:url('../img/p1.jpg');"></div>
                    <div class="carousel-caption">
                        <h2>Adopciones</h2>
                    </div>
                </div>
                <div class="item">
                    <div class="fill" style="background-image:url('../img/p3.jpg');"></div>
                    <div class="carousel-caption">
                        <h2>Eventos</h2>
                    </div>
                </div>
                <div class="item">
                    <div class="fill" style="background-image:url('../img/p4.jpg');"></div>
                    <div class="carousel-caption">
                        <h2>Testimonios</h2>
                    </div>
                </div>
            </div>

            <!-- Controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="icon-prev"></span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span class="icon-next"></span>
            </a>
        </header>

        <!-- Page Content -->
        <div class="container">
            <!-- Marketing Icons Section -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 align="center" class="page-header"><img src="../img/textoPeluditos.png" width = "500" height="170" alt="" />
                        <ul class="list-inline">
                                <li><div class="fl_left"><p align="center"><a href="http://www.facebook.com/peluditoss"><img src="../img/face.png" width = "50" height="50" alt="" /></a></div>
                                </li>
                                <li><div class="fl_left"><p align="center"><a href="http://www.twitter.com"><img src="../img/twitter.png" width = "50" height="50" alt="" /></a></div>
                                </li>
                            </ul></h1>
                    <h3 align="center">Por que la mascota es la alegría de la vida.</h3>
                    <br>
                </div>
                <?php
                    contenido();
                ?>
            </div>
            <!-- /.row -->

            <!-- Portfolio Section -->
            
            <hr>


            <!-- Footer -->
            <footer>
                <div class="row">
                    <div class="col-lg-12">
                        <p>&copy; Peluditos 2015</p>
                        <?php
                            cerrar_sesion();
                        ?>
                    </div>
                </div>
            </footer>

        </div>
        <!-- /.container -->

        <!-- jQuery Version 1.11.0 -->
        <script src="js/jquery-1.11.0.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>

        <!-- Script to Activate the Carousel -->
        <script>
        $('.carousel').carousel({
            interval: 5000 //changes the speed
        })
        </script>

    </body>
</html>