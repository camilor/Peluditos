<?php
    session_start();
    require_once 'conexion.php';
    require 'oper.php';
?>

<!DOCTYPE html>
<html>
    <head>
        <?php
            head();
            if($_GET['usr'] == NULL or isset($_SESSION['admin_peluditos']) != $_GET['usr'])
            {
                header('Location: /Peluditos.com/Admin');
            }
            echo "<title>$_SESSION[admin_peluditos]</title>";
        ?>
    </head>

    <body>
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <?php
                    info();
                    error();
                ?>
            </div>
            <!-- /.container -->
        </nav>

        <!-- Page Content -->
        <div class="container">
            <!-- Marketing Icons Section -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 align="center" class="page-header"><img src="../img/textoPeluditos.png" width = "500" height="170" alt="" /></h1>
                    <h3 align="center">Gracias por formar parte de nosotros, tu ayuda es de gran importancia para la causa.</h3>
                    <br>
                </div>
            </div>
            <!-- /.row -->

            <div class='row'>
                <?php
                    $consulta = mysqli_query($conexion, "SELECT *
                                                         FROM Administrador
                                                         WHERE Usuario = '".$_SESSION['admin_peluditos']."'")
                                or die ('Fallo en la consulta');

                    $fila = mysqli_fetch_array($consulta);

                    echo "  <div class='col-md-6'>
                                <h2>Tus datos</h2>
                                <br>";

                    if($fila['Foto'] == "")
                    {
                        echo        "<img class='img-responsive' src='../img/sinFoto.png' alt='' style='width:300px; height:300px; border:1 solid #122235'>";
                    }

                    else
                    {
                        echo        "<img class='img-responsive' src='../users/$fila[Foto]' alt='' style='width:300px; height:300px; border:1 solid #122235'>";
                    }
                    
                    echo        "<br>
                                <p><b>Usuario:</b>    ".$fila['Usuario']."</p>
                                <p><b>Nombre:</b>     ".$fila['Nombre']."</p>
                                <p><b>E-mail:</b>     ".$fila['Email']."</p>
                                    <br>
                            </div>";
                ?>

                <div class='col-md-6'>
                    <form action='' method='post' ENCTYPE='multipart/form-data'>
                        <h3>Cambiar tus datos</h3>
                        <br>
                            <table>
                                <tr>
                                    <td>
                                        <label>Nombre:</label>
                                    </td>
                                    <td>
                                        <font color='white'>..........</font>
                                    </td>
                                    <td>
                                        <input type='text' class="form-control" name='nombre'  size='30'>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label>Foto</label>
                                    </td>
                                    <td>

                                    </td>
                                    <td>
                                        <form id='perfil' name='perfil' enctype='multipart/form-data' method='post' action=''>
                                            <input type='hidden' class="form-control" name='MAX_FILE_SIZE' value='2000000' />
                                            <input type='file' class="form-control" name='imag' id='imag' size='30'/>
                                        </form>
                                    </td>
                                </tr>
                                <tr>
                                <td>
                                    <input type='submit' class="btn btn-primary" name='datos' id='datos' value='Cambiar'/>
                                </td>
                                <td>

                                </td>
                                <td>

                                </td>
                            </tr>
                            </table>
                        <br>
                        <h3>Cambiar contraseña</h3>
                        <table>
                            <tr>
                                <td>
                                    <label>*Contraseña actual:</label>
                                </td>
                                <td>
                                    <font color='white'>.......</font>
                                </td>
                                <td>
                                    <input type='password' class='form-control' name='actual' size='30' />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>*Nueva contraseña:</label>
                                </td>
                                <td>

                                </td>
                                <td>
                                    <input type='password' class='form-control' name='nueva' size='30' />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>*Confirmar contraseña:</label>
                                </td>
                                <td>

                                </td>
                                <td>
                                    <input type='password' class='form-control' name='confirmar' size='30' />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type='submit' class="btn btn-primary" name='psw' id='psw' value='Cambiar'/>
                                </td>
                                <td>

                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                        <?php
                            if(isset($_REQUEST['datos']))
                            {
                                if($_REQUEST['nombre'] != "" and $_FILES['imag']['name'] == "")
                                {
                                    $consulta = mysqli_query($conexion, "   UPDATE Administrador
                                                                            SET Nombre = '$_REQUEST[nombre]'
                                                                            WHERE Usuario = '$_GET[usr]'")
                                        or die ('Fallo en la consulta');

                                    header("Location: /Peluditos.com/Admin/Perfil.php?usr=".$_GET['usr']);
                                }

                                elseif($_FILES['imag']['name'] != "" and $_REQUEST['nombre'] == "")
                                {
                                    move_uploaded_file($_FILES['imag']['tmp_name'],"../users/".$_FILES['imag']['name']);

                                    $consulta = mysqli_query($conexion, "   UPDATE Administrador
                                                                            SET Foto = '".$_FILES['imag']['name']."'
                                                                            WHERE Usuario = '".$_GET['usr']."'")
                                        or die ('Fallo en la consulta');

                                    header("Location: /Peluditos.com/Admin/Perfil.php?usr=".$_GET['usr']);
                                }

                                elseif($_REQUEST['nombre'] == "" and $_FILES['imag']['name'] =="")
                                {
                                    echo "  <script language='JavaScript'> 
                                                alert('Campos vacíos, no se modificó nada.'); 
                                            </script>";
                                }

                                else
                                {
                                    move_uploaded_file($_FILES['imag']['tmp_name'],"../users/".$_FILES['imag']['name']);

                                    $consulta = mysqli_query($conexion, "   UPDATE Administrador
                                                                            SET Nombre = '".$_REQUEST['nombre']."', Foto = '".$_FILES['imag']['name']."'
                                                                            WHERE Usuario = '".$_GET['usr']."'")
                                        or die ('Fallo en la consulta');

                                    header("Location: /Peluditos.com/Admin/Perfil.php?usr=".$_GET['usr']);
                                }
                            }

                            if(isset($_REQUEST['psw'])) 
                            {
                                if($_REQUEST['actual'] != "" and $_REQUEST['nueva'] != "" and $_REQUEST['confirmar'] != "")
                                {
                                    $consulta = mysqli_query($conexion, "SELECT Password
                                                                         FROM Administrador
                                                                         WHERE Usuario = '".$_GET['usr']."'")
                                        or die ('Fallo en la consulta');

                                    $fila = mysqli_fetch_array($consulta);

                                    if(md5($_REQUEST['actual']) == $fila['Password'])
                                    {
                                        if(md5($_REQUEST['nueva']) == md5($_REQUEST['confirmar']))
                                        {
                                            $consulta2 = mysqli_query($conexion, "  UPDATE Administrador
                                                                                    SET Password = '".md5($_REQUEST['nueva'])."'
                                                                                    WHERE Usuario = '".$_GET['usr']."'")
                                                or die ('Fallo en la consulta');

                                            echo "  <script language='JavaScript'> 
                                                        alert('Contraseña cambiada, la próxima vez que inicie sesión, deberá proporcionar la nueva contraseña.'); 
                                                    </script>";
                                        }

                                        else
                                        {
                                            echo "  <script language='JavaScript'> 
                                                        alert('La contraseñas nuevas no coinciden.'); 
                                                    </script>";
                                        }
                                    }

                                    else
                                    {
                                        echo "  <script language='JavaScript'> 
                                                    alert('La contraseña actual es incorrecta.'); 
                                                </script>";
                                    }
                                }

                                else
                                {
                                    echo "  <script language='JavaScript'> 
                                                alert('Campos obligatorios vacíos.'); 
                                            </script>";
                                }
                            }
                        ?>
                        <br>
                        <br>
                    </form>
                </div>
            </div>
            <!-- /.row -->
            
            <hr>


            <!-- Footer -->
            <footer>
                <div class="row">
                    <div class="col-lg-12">
                        <p>&copy; Peluditos 2015</p>
                        <?php
                            cerrar_sesion();
                        ?>
                    </div>
                </div>
            </footer>

        </div>
        <!-- /.container -->

        <!-- jQuery Version 1.11.0 -->
        <script src="js/jquery-1.11.0.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>

        <!-- Script to Activate the Carousel -->
        <script>
        $('.carousel').carousel({
            interval: 5000 //changes the speed
        })
        </script>

    </body>
</html>