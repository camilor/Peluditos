<?php
    require 'oper.php';
?>

<!DOCTYPE html>
<html>
    <head>
        <?php
            head();
        ?>

        <title>Acerca de</title>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>

        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <?php
                    info();
                ?>
            </div>
            <!-- /.container -->
        </nav>

        <!-- Page Content -->
        <div class="container">

            <!-- Page Heading/Breadcrumbs -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><img src="img/textoPeluditos.PNG" width = "500" height="170" alt="" />
                        <small>Sobre nosotros:</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="/Peluditos.com">Inicio</a>
                        </li>
                        <li class="active">¿Quiénes Somos?</li>
                    </ol>
                </div>
            </div>
            <!-- /.row -->

            <!-- Intro Content -->
            <div class="row">
                <div class="col-md-6">
                    <img class="img-responsive" src="img/logoPeluditos.png" alt="">
                </div>
                <div class="col-md-6">
                    <h2>Acerca de Peluditos</h2>
                    <p>Esta plataforma ha sido diseñada con el objetivo de brindar un servicio a la comunidad y ver por el bienestar de los animalitos que necesitan   un hogar, con una familia que se preocupe por cubrir sus necesidades básicas y les brinde un hogar en donde se sientan queridos y protegidos.</p>
                    <p>Si tú eres esa persona especial que puede brindarle un hogar a un Peludito, puedes informarte sobre la adopción de algún amiguito peludo, los eventos que se realizarán, testimonios de las personas que realizaron una adopción y además tú podrás crear reportes de algún Peludito que esté herido, extraviado, etc.</p>
                    <p>Si tienes Peluditos que requieren nuevo hogar, aquí lo podrás poner en adopción y encontrarle una familia; o si quieres tener un Peludito puedes ver la galería de fotos de nuestros amigos peludos.</p>
                </div>
            </div>
            <!-- /.row -->

            <!-- Team Members -->
           
            <!-- /.row -->


            <hr>

            <!-- Footer -->
            <footer>
                <div class="row">
                    <div class="col-lg-12">
                        <p>&copy; Peluditos 2015</p>
                    </div>
                </div>
            </footer>

        </div>
        <!-- /.container -->

        <!-- jQuery Version 1.11.0 -->
        <script src="js/jquery-1.11.0.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>
