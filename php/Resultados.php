<?php
    session_start();
    require_once 'conexion.php';
    require 'oper.php';
?>

<!DOCTYPE html>
<html>
    <head>
        <?php
            head();
            echo "<title>Búsqueda</title>";
        ?>

        <script language="Javascript"> 
            function eliminar_adopcion(id)
            {
                confirmar = confirm('¿Deseas eliminar esta Adopción?');
                if(confirmar)
                {
                    alert('Adopcion eliminado satisfactoriamente.');
                    window.location.href = 'Borrar_Adopcion.php?id_ad=' + id;
                }

                else
                {
                    alert('Nada se eliminó.');
                }
            }

            function eliminar_evento(id)
            {
                confirmar = confirm('¿Deseas eliminar este Evento?');
                if(confirmar)
                {
                    alert('Evento eliminado satisfactoriamente.');
                    window.location.href = 'Borrar_Evento.php?id_ev=' + id;
                }

                else
                {
                    alert('Nada se eliminó.');
                }
            }

            function eliminar_reporte(id)
            {
                confirmar = confirm('¿Deseas eliminar este Reporte?');
                if(confirmar)
                {
                    alert('Reporte eliminado satisfactoriamente.');
                    window.location.href = 'Borrar_Reporte.php?id_re=' + id;
                }

                else
                {
                    alert('Nada se eliminó.');
                }
            }

            function eliminar_testimonio(id)
            {
                confirmar = confirm('¿Deseas eliminar este Testimonio?');
                if(confirmar)
                {
                    alert('Evento eliminado satisfactoriamente.');
                    window.location.href = 'Borrar_Testimonio.php?id_te=' + id;
                }

                else
                {
                    alert('Nada se eliminó.');
                }
            }
        </script>
    </head>

    <body>
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <?php
                    info();
                    error();
                ?>
            </div>
            <!-- /.container -->
        </nav>

        <!-- Page Content -->
        <div class="container">
            <!-- Marketing Icons Section -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 align="center" class="page-header"><img src="../img/textoPeluditos.png" width = "500" height="170" alt="" /></h1>
                    <?php
                        echo "<h3 align='center'>Resultados sobre $_GET[result]...</h3>";
                    ?>
                    <br>
                </div>
            </div>
            <!-- /.row -->

            <?php
                    $bus_fecha = substr($_GET['result'], 6).'-'.substr($_GET['result'], 3, 2).'-'.substr($_GET['result'], 0, 2);

                    $consulta = mysqli_query($conexion, "SELECT *
                                                         FROM Reporte as r, Ubicacion as u
                                                         WHERE r.Id_Ubicacion = u.Id_Ubicacion and (r.Id_Reporte = '".$_GET['result']."' or u.Estado = '".$_GET['result']."' or r.Usuario = '".$_GET['result']."' or r.Telefono = '".$_GET['result']."' or r.Email = '".$_GET['result']."' or r.Telefono = '".$_GET['result']."' or r.Asunto = '".$_GET['result']."' or r.Vista = '".$_GET['result']."' or r.Grado = '".$_GET['result']."' or r.Fecha = '".$bus_fecha."' or r.Status = '".$_GET['result']."')
                                                         ORDER BY Fecha DESC")
                        or die ('Fallo en la consulta');
                                
                    $nfilas = mysqli_num_rows($consulta);

                    if($nfilas > 0)
                    {
                        for($i = 0; $i < $nfilas; $i++)
                        {
                            $fila = mysqli_fetch_array($consulta);
                            $fecha = substr($fila['Fecha'], 8).'/'.substr($fila['Fecha'], 5, 2).'/'.substr($fila['Fecha'], 0, 4);

                            echo "<div class='row'>
                                    <h2 class='page-header'>ID: ".$fila['Id_Reporte']."</h2>
                                    <div class='col-md-6'>
                                        <div class='thumbnail'>";

                            if($fila['Foto'] == "")
                            {
                                echo        "<img class='img-responsive' src='../img/sinFoto.png' alt='' style='width:400px; height:350px; border:1 solid #122235'>";
                            }

                            else
                            {
                                echo        "<img class='img-responsive' src='../images/$fila[Foto]' alt='' style='width:400px; height:350px; border:1 solid #122235'>";
                            }


                            echo            "<button type='button' class='btn btn-primary' style='Position:Absolute; left:88%; top:83%' name='regresar' id='regresar' value='' onclick='eliminar_reporte(".$fila['Id_Reporte'].")'><img src='../img/eliminar.png' style='width:20px; height:20px;'></button>
                                        </div>
                                    </div>
                                    <div class='col-md-6'>";

                            if($fila['Grado'] == "Emergencia")
                            {
                                echo "  <img class='img-responsive' src='../img/emergencia.gif' alt='' style='width:250px; height:100px; Position:Absolute; left:70%; top:-3%'/>";
                            }

                            echo "      <h2>Título: ".$fila['Asunto']."</h2>
                                        <h3>Datos del Informante</h3>
                                        <p><b>Nombre:</b>       ".$fila['Usuario']."</p>
                                        <p><b>Teléfono:</b>        ".$fila['Telefono']."</p>
                                        <p><b>E-mail:</b>       ".$fila['Email']."</p><h3>Detalles</h3>
                                        <p><b>Descripción:</b>     ".$fila['Descripcion']."</p>
                                        <p><b>Ubicación:</b>     ".$fila['Estado']."</p>
                                        <br>
                                        <p><b>Fecha de publicación:</b> ".$fecha."</p>";

                            if($fila['Status'] == 'Activo')
                            {
                                echo "  <br>
                                        <p><b>Status: <font color='#088A08'>".$fila['Status']."</font></b></p>";
                            }

                            elseif ($fila['Status'] == 'Concluído')
                            {
                                echo "  <br>
                                        <p><b>Status: <font color='#DF7401'>".$fila['Status']."</font></b></p>";
                            }

                            elseif ($fila['Status'] == 'Expirado')
                            {
                                echo "  <br>
                                        <p><b>Status: <font color='#DF0101'>".$fila['Status']."</font></b></p>";
                            }
                            
                            echo "      <br>
                                    </div>
                                </div>";
                       }
                    }

                    $consulta = mysqli_query($conexion, "SELECT *
                                                         FROM Adopcion as a, Ubicacion as u
                                                         WHERE a.Id_Ubicacion = u.Id_Ubicacion and (a.Id_Adopcion = '".$_GET['result']."' or u.Estado = '".$_GET['result']."' or a.Usuario = '".$_GET['result']."' or a.Email = '".$_GET['result']."' or a.Telefono = '".$_GET['result']."' or a.Mascota = '".$_GET['result']."' or a.Tipo = '".$_GET['result']."' or a.Genero = '".$_GET['result']."' or a.Raza = '".$_GET['result']."' or a.Color = '".$_GET['result']."' or a.Fecha = '".$bus_fecha."' or a.Status = '".$_GET['result']."')
                                                         ORDER BY Fecha DESC")
                                or die ('Fallo en la consulta');

                    $nfilas = mysqli_num_rows($consulta);

                    if($nfilas > 0)
                    {
                        for($i = 0; $i < $nfilas; $i++)
                        {
                            $fila = mysqli_fetch_array($consulta);
                            $fecha = substr($fila['Fecha'], 8).'/'.substr($fila['Fecha'], 5, 2).'/'.substr($fila['Fecha'], 0, 4);

                            echo "  <div class='row'>
                                        <h2 class='page-header'>ID: ".$fila['Id_Adopcion']."</h2>
                                        <div class='col-md-6'>
                                            <div class='thumbnail'>";

                            if($fila['Foto'] == "")
                            {
                                echo        "<img class='img-responsive' src='../img/sinFoto.png' alt='' style='width:400px; height:350px; border:1 solid #122235'>";
                            }

                            else
                            {
                                echo        "<img class='img-responsive' src='../images/$fila[Foto]' alt='' style='width:400px; height:350px; border:1 solid #122235'>";
                            }


                            echo            "<button type='button' class='btn btn-primary' style='Position:Absolute; left:88%; top:83%' name='regresar' id='regresar' value='' onclick='eliminar_adopcion(".$fila['Id_Adopcion'].")'><img src='../img/eliminar.png' style='width:20px; height:20px;'></button> 
                                            </div>
                                        </div>
                                        <div class='col-md-6'>
                                            <h3>Datos de Contacto</h3>
                                            <p><b>Dueño:</b>       ".$fila['Usuario']."</p>
                                            <p><b>Teléfono:</b>        ".$fila['Telefono']."</p>
                                            <p><b>E-mail:</b>       ".$fila['Email']."</p>
                                            <h3>Datos de la Mascota</h3>
                                            <p><b>Nombre:</b>      ".$fila['Mascota']."</p>
                                            <p><b>Tipo:</b>        ".$fila['Tipo']."</p>
                                            <p><b>Raza:</b>        ".$fila['Raza']."</p>
                                            <p><b>Color:</b>       ".$fila['Color']."</p>
                                            <p><b>Género:</b>      ".$fila['Genero']."</p>
                                            <p><b>Descripción:</b>     ".$fila['Descripcion']."</p>
                                            <p><b>Ubicación:</b>     ".$fila['Estado']."</p>
                                            <br>
                                            <p><b>Fecha de publicación:</b> ".$fecha."</p>";

                            if($fila['Status'] == 'Activo')
                            {
                                echo "  <br>
                                        <p><b>Status: <font color='#088A08'>".$fila['Status']."</font></b></p>";
                            }

                            elseif ($fila['Status'] == 'Concluído')
                            {
                                echo "  <br>
                                        <p><b>Status: <font color='#DF7401'>".$fila['Status']."</font></b></p>";
                            }

                            elseif ($fila['Status'] == 'Expirado')
                            {
                                echo "  <br>
                                        <p><b>Status: <font color='#DF0101'>".$fila['Status']."</font></b></p>";
                            }
                            
                            echo "  </div>
                                </div>";
                        }
                    }

                    $consulta = mysqli_query($conexion, "SELECT *
                                                         FROM Evento as e, Ubicacion as u
                                                         WHERE e.Id_Ubicacion = u.Id_Ubicacion and (e.Id_Evento = '".$_GET['result']."' or u.Estado = '".$_GET['result']."' or e.Nombre = '".$_GET['result']."' or e.Fecha_Evento = '".$bus_fecha."' or e.Usuario = '".$_GET['result']."' or e.Telefono = '".$_GET['result']."' or e.Email = '".$_GET['result']."' or e.Fecha = '".$bus_fecha."' or e.Status = '".$_GET['result']."')
                                                         ORDER BY Fecha DESC")
                                or die ('Fallo en la consulta');

                    $nfilas = mysqli_num_rows($consulta);

                    if($nfilas > 0)
                    {
                        for($i = 0; $i < $nfilas; $i++)
                        {
                            $fila = mysqli_fetch_array($consulta);
                            $fecha = substr($fila['Fecha'], 8).'/'.substr($fila['Fecha'], 5, 2).'/'.substr($fila['Fecha'], 0, 4);
                            $fec_eve = substr($fila['Fecha_Evento'], 8).'/'.substr($fila['Fecha_Evento'], 5, 2).'/'.substr($fila['Fecha_Evento'], 0, 4);

                            echo "  <div class='row'>
                                        <h2 class='page-header'>ID: ".$fila['Id_Evento']."</h2>
                                        <div class='col-md-6'>
                                            <div class='thumbnail'>";

                            if($fila['Foto'] == "")
                            {
                                echo        "<img class='img-responsive' src='../img/sinFoto.png' alt='' style='width:400px; height:350px; border:1 solid #122235'>";
                            }

                            else
                            {
                                echo        "<img class='img-responsive' src='../images/$fila[Foto]' alt='' style='width:400px; height:350px; border:1 solid #122235'>";
                            }


                            echo            "<button type='button' class='btn btn-primary' style='Position:Absolute; left:88%; top:83%' name='regresar' id='regresar' value='' onclick='eliminar_evento(".$fila['Id_Evento'].")'><img src='../img/eliminar.png' style='width:20px; height:20px;'></button>
                                                <!--<a href = 'eliminar_persona.php?id=".$fila['Id_Evento']."'><img src = '../img/eliminar.png' height = 35 width = 35></a>-->
                                            </div>
                                        </div>
                                        <div class='col-md-6'>
                                            <h3>Datos de Contacto</h3>
                                            <p><b>Organizador:</b>       ".$fila['Usuario']."</p>
                                            <p><b>Teléfono:</b>        ".$fila['Telefono']."</p>
                                            <p><b>E-mail:</b>       ".$fila['Email']."</p>
                                            <br>
                                            <h3>".$fila['Nombre']."</h3>
                                            <p><b>Descripción:</b>     ".$fila['Descripcion']."</p>
                                            <p><b>Fecha del evento:</b>     ".$fec_eve."</p>
                                            <p><b>Ubicación:</b>     ".$fila['Estado']."</p>
                                            <br>
                                            <p><b>Fecha de publicación:</b> ".$fecha."</p>";

                            if($fila['Status'] == 'Activo')
                            {
                                echo "  <br>
                                        <p><b>Status: <font color='#088A08'>".$fila['Status']."</font></b></p>";
                            }

                            elseif ($fila['Status'] == 'Concluído')
                            {
                                echo "  <br>
                                        <p><b>Status: <font color='#DF7401'>".$fila['Status']."</font></b></p>";
                            }
                    
                            echo "  </div>
                                </div>";
                        }
                    }

                    $consulta = mysqli_query($conexion, "SELECT *
                                                         FROM Testimonio as t, Ubicacion as u
                                                         WHERE t.Id_Ubicacion = u.Id_Ubicacion and (t.Id_Testimonio = '".$_GET['result']."' or u.Estado = '".$_GET['result']."' or t.Usuario = '".$_GET['result']."' or t.Mascota = '".$_GET['result']."' or t.Titulo = '".$_GET['result']."' or t.Fecha = '".$bus_fecha."')
                                                         ORDER BY Fecha DESC")
                        or die ('Fallo en la consulta');
                                
                    $nfilas = mysqli_num_rows($consulta);

                    if($nfilas > 0)
                    {
                        for($i = 0; $i < $nfilas; $i++)
                        {
                            $fila = mysqli_fetch_array($consulta);
                            $fecha = substr($fila['Fecha'], 8).'/'.substr($fila['Fecha'], 5, 2).'/'.substr($fila['Fecha'], 0, 4);

                            echo "<div class='row'>
                                    <h2 class='page-header'>ID: ".$fila['Id_Testimonio']."</h2>
                                    <div class='col-md-6'>
                                        <div class='thumbnail'>";

                            if($fila['Foto'] == "")
                            {
                                echo        "<img class='img-responsive' src='../img/sinFoto.png' alt='' style='width:400px; height:350px; border:1 solid #122235'>";
                            }

                            else
                            {
                                echo        "<img class='img-responsive' src='../images/$fila[Foto]' alt='' style='width:400px; height:350px; border:1 solid #122235'>";
                            }


                            echo            "<button type='button' class='btn btn-primary' style='Position:Absolute; left:88%; top:83%' name='regresar' id='regresar' value='' onclick='eliminar_testimonio(".$fila['Id_Testimonio'].")'><img src='../img/eliminar.png' style='width:20px; height:20px;'></button>
                                        </div>
                                    </div>
                                    <div class='col-md-6'>
                                        <h2>Historia de $fila[Usuario] y $fila[Mascota]</h2>
                                        <p><b>Título:</b> ".$fila['Titulo']."</p>
                                        <p><b>Descripción:</b>     ".$fila['Descripcion']."</p>
                                        <br>
                                        <h4><b>Contacto</b></h4>
                                        <p><b>E-mail:</b>     $fila[Email]</p>
                                        <p><b>Ubicación:</b>     ".$fila['Estado']."</p>
                                        <p><b>Fecha de publicación:</b> ".$fecha."</p>
                                        <br>
                                    </div>
                                </div>";
                        }
                    }
            ?>

            <div class='row'>
                
            </div>
            <!-- /.row -->
            
            <hr>


            <!-- Footer -->
            <footer>
                <div class="row">
                    <div class="col-lg-12">
                        <p>&copy; Peluditos 2015</p>
                        <?php
                            cerrar_sesion();
                        ?>
                    </div>
                </div>
            </footer>

        </div>
        <!-- /.container -->

        <!-- jQuery Version 1.11.0 -->
        <script src="js/jquery-1.11.0.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>

        <!-- Script to Activate the Carousel -->
        <script>
        $('.carousel').carousel({
            interval: 5000 //changes the speed
        })
        </script>

    </body>
</html>