<?php
    require_once 'conexion.php';
    //require 'procs.php';
    require_once('recaptchalib.php');
    $publickey = '6Ldu18gSAAAAAM-ucC-EwTd-i-eGSwHWFZxsDdPt';
    require 'oper.php';
?>

<!DOCTYPE html>
<html>
    <head>
        <?php
            head();
        ?>

        <title>Registrar Adopción</title>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src='https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js'></script>
            <script src='https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js'></script>
        <![endif]-->
    </head>

    <body>

        <nav class='navbar navbar-inverse navbar-fixed-top' role='navigation'>
            <div class='container'>
                <!-- Brand and toggle get grouped for better mobile display -->
                <?php
                    info();
                ?>
            </div>
            <!-- /.container -->
        </nav>

        <!-- Page Content -->
        <div class='container'>

            <!-- Page Heading/Breadcrumbs -->
            <div class='row'>
                <div class='col-lg-12'>
                   <h1 class='page-header'>Si tienes una mascota que necesita ser adoptada... </h1>
                    <ol class='breadcrumb'>
                        <li><a href='/Peluditos.com'>Inicio</a>
                        </li>
                        <li><a href='Adopciones.php'>Mascotas</a>
                        </li>
                        <li class='active'>Buscame un dueño</li>
                    </ol>
                </div>
            </div>

            <!-- /.row -->

            <!-- Intro Content -->



            <div class='row'>
                <div class="col-lg-12"> 
                    <h1 align="center">Te ayudamos a encontrarle un nuevo hogar</h1>
                    <h4 align="center">Aquí podrás crear una solicitud para dar en adopción una mascota y encontrarle un nuevo hogar donde sea feliz.</h4>
                </div>

                <form action='' method='post' ENCTYPE='multipart/form-data'>
                <div class='col-md-6'>
                    <h2>Contacto</h2>
                    <table>
                        <tr>
                            <td>
                                <label>*Nombre Completo:</label>
                            </td>
                            <td>
                                <font color='white'>..........</font>
                            </td>
                            <td>
                                <input type='text' class="form-control" name='usuario' size='30'>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>*E-Mail:</label>
                            </td>
                            <td>

                            </td>
                            <td>
                                <input type='text' class="form-control" name='email' size='30'>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Teléfono:</label>
                            </td>
                            <td>

                            </td>
                            <td>
                                <input type='text' class="form-control" name='telefono' size='30'>
                            </td>
                        </tr>
                    </table>

                    <h2>Ubicación</h2>
                    <table>
                        <tr>
                            <td>
                                <label>*Estado:</label>
                            </td>
                            <td>
                                <font color='white'>..........</font>
                            </td>
                            <td>
                                <select name='estado' class="form-control">
                                    <option value=0>--Seleccione una opción--</option>
                                    <?php
                                        $consulta = mysqli_query($conexion, "SELECT * FROM Ubicacion ORDER BY Estado")
                                            or die ("Fallo en la consulta");
                                                    
                                        $nfilas = mysqli_num_rows($consulta);
                                                                    
                                        if($nfilas > 0)
                                        {
                                            for($i = 0; $i < $nfilas; $i++)
                                            {
                                                $fila = mysqli_fetch_array($consulta);
                                                echo "<option value=".$fila['Id_Ubicacion'].">".$fila['Estado']."</option>";
                                            }
                                        }
                                    ?>
                                </select>
                            </td>
                        </tr>
                    </table>
                </div>
                            
                            
                <div class='col-md-6'>
                    <h2>Mascota</h2>
                    <table>
                        <tr>
                            <td>
                                <label>Nombre:</label>
                            </td>
                            <td>
                                <font color='white'>..........</font>
                            </td>
                            <td>
                                <input type='text' class="form-control" name='mascota'  size='30'>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>*Tipo:</label>
                            </td>
                            <td>

                            </td>
                            <td>
                                <select class="form-control" name='tipo' onchange="if(this.value=='Otro') document.getElementById('tipo_otro').disabled = false; if(this.value!='Otro') document.getElementById('tipo_otro').disabled = true">
                                	<option value='0'>--Seleccione una opción--</option>
                                    <option value='Perro'>Perro</option> 
                                    <option value='Gato'>Gato</option> 
                                    <option value='Ave'>Ave</option>
                                    <option value='Otro'>Otro</option>
                                </select>
                                <input type='text' class="form-control" id='tipo_otro' name='otro' size='30' disabled='disabled' placeholder="Especifica el tipo">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>*Género:</label>
                            </td>
                            <td>

                            </td>
                            <td>
                                <select class="form-control" name='genero'>
                                	<option value='0'>--Seleccione una opción--</option>
                                    <option value='Hembra'>Hembra</option> 
                                    <option value='Macho'>Macho</option> 
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Raza:</label>
                            </td>
                            <td>

                            </td>
                            <td>
                                <input type='text' class="form-control" name='raza' size='30'>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Color:</label>
                            </td>
                            <td>

                            </td>
                            <td>
                                <input type='text' class="form-control" name='color' size='30'>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Descripción:</label>
                            </td>
                            <td>

                            </td>
                            <td>
                                <textarea class="form-control" name='descripcion' cols='31' rows='3' id='t1'></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for='imagen'>Foto</label>
                            </td>
                            <td>

                            </td>
                            <td>
                                <form id='buscame' name='buscame' enctype='multipart/form-data' method='post' action=''>
                                    <input type='hidden' class="form-control" name='MAX_FILE_SIZE' value='2000000' />
                                    <input type='file' class="form-control" name='imag' id='imag' size='30'/>
                                </form>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <br>
                </div>
                <div class="col-lg-12" align="center">
                    <?php
                        $error = null;
                        echo recaptcha_get_html($publickey, $error);
                    ?>
                    <input type='submit' class="btn btn-primary" name='registrar' id='registrar' value='Registrar'/>
                    <input type='submit' class="btn btn-primary" name='cancelar' id='cancelar' value='Cancelar'/>
                    <?php   
                        $privatekey = '6Ldu18gSAAAAALGxra-B7SGjk2jIYGY_Pa1yeYtF';
                        # the response from reCAPTCHA
                        $resp = null;
                        # the error code from reCAPTCHA, if any
                        $error = null;

                        if(isset($_REQUEST['registrar']))
                        {
                            date_default_timezone_set('America/Mexico_City');
                            $fecha = date("Y-m-d");

                            if($_REQUEST["recaptcha_response_field"])
                            {
                                $resp = recaptcha_check_answer ($privatekey,
                                                                $_SERVER["REMOTE_ADDR"],
                                                                $_POST["recaptcha_challenge_field"],
                                                                $_POST["recaptcha_response_field"]);
                                if($resp->is_valid)
                                {
                                    if($_REQUEST['usuario'] != "" and $_REQUEST['email'] != "" and $_REQUEST['estado'] != 0 and $_REQUEST['tipo'] != "0" and $_REQUEST['genero'] != "0")
                                    {
                                        if($_REQUEST['tipo'] != "Otro")
                                        {
                                            move_uploaded_file($_FILES['imag']['tmp_name'],"images/".$_FILES['imag']['name']);

                                            $string = "call reg_adop(".$_REQUEST['estado'].", '".$_REQUEST['usuario']."', '".$_REQUEST['telefono']."', '".$_REQUEST['email']."', '".$_REQUEST['descripcion']."', '".$_REQUEST['mascota']."', '".$_REQUEST['tipo']."', '".$_REQUEST['genero']."', '".$_REQUEST['raza']."', '".$_REQUEST['color']."', '".$_FILES['imag']['name']."', '".$fecha."', 'Activo')";
                                            $r = mysqli_query($conexion, $string)
                                               or die("Fallo en el procedimiento");

                                            header('Location: Adopciones.php');
                                        
                                        }

                                        else
                                        {
                                            if($_REQUEST['otro'] == "")
                                            {
                                                echo "*Campos obligatorios vacíos";
                                            }

                                            else
                                            {
                                                move_uploaded_file($_FILES['imag']['tmp_name'],"images/".$_FILES['imag']['name']);

                                                $string = "call reg_adop(".$_REQUEST['estado'].", '".$_REQUEST['usuario']."', '".$_REQUEST['telefono']."', '".$_REQUEST['email']."', '".$_REQUEST['descripcion']."', '".$_REQUEST['mascota']."', '".$_REQUEST['otro']."', '".$_REQUEST['genero']."', '".$_REQUEST['raza']."', '".$_REQUEST['color']."', '".$_FILES['imag']['name']."', '".$fecha."', 'Activo')";
                                                $r = mysqli_query($conexion, $string)
                                                    or die("Fallo en el procedimiento");

                                                header('Location: Adopciones.php');
                                            }
                                        }
                                    }
                                
                                    else
                                    {
                                        echo "*Campos obligatorios vacíos";
                                    }
                                }
                                        
                                else
                                {
                                    echo "Capcha Incorrecto";
                                }   
                            }
                        }

                        if(isset($_REQUEST['cancelar'])) 
                        {
                            header('Location: Adopciones.php');
                        }
                    ?>
                </div>
                </form>
            </div>
            <!-- /.row -->

            <!-- Team Members -->
            <div class='row'>
            </div>
            <!-- /.row -->

            <hr>

            <!-- Footer -->
            <footer>
                <div class='row'>
                    <div class='col-lg-12'>
                        <p>&copy; Peluditos 2015</p>
                    </div>
                </div>
            </footer>
        </div>
        <!-- /.container -->

        <!-- jQuery Version 1.11.0 -->
        <script src='js/jquery-1.11.0.js'></script>

        <!-- Bootstrap Core JavaScript -->
        <script src='js/bootstrap.min.js'></script>
    </body>
</html>