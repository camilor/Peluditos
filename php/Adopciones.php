<?php
    session_start();
    require_once 'conexion.php';
    require 'oper.php';
?>

<!DOCTYPE html>
<html>
    <head>
        <?php
            head();
        ?>

        <title>Mascotas en adopción</title>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <script language="Javascript"> 
            function eliminar_adopcion(id)
            {
                confirmar = confirm('¿Deseas eliminar esta Adopción?');
                if(confirmar)
                {
                    alert('Adopcion eliminado satisfactoriamente.');
                    window.location.href = 'Borrar_Adopcion.php?id_ad=' + id;
                }

                else
                {
                    alert('Nada se eliminó.');
                }
            }
        </script>
    </head>

    <body>
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <?php
                    acceso();
                    info();
                ?>
            </div>
            <!-- /.container -->
        </nav>

        <!-- Page Content -->
        <div class="container">
            <!-- Page Heading/Breadcrumbs -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Mascotas en adopción</h1>
                    <ol class="breadcrumb">
                        <li><a href="/Peluditos.com/Admin">Inicio</a>
                        </li>
                        <li class="active">Mascotas</li>
                    </ol>
                </div>
            </div>
            <!-- /.row -->

            <!-- Intro Content -->
            <div class="row">
                <div class="col-lg-12">
                    <p align="center">
                        <img class="img-responsive" src="../img/Juntos.jpg" alt="">
                    </p>
                </div>
            </div>
            <!-- /.row -->

            <!-- Team Members -->
            <div class="row">
                <div class="col-lg-12">
                </div>
                <?php
                    $consulta = mysqli_query($conexion, "SELECT *
                                                         FROM Adopcion as a, Ubicacion as u
                                                         WHERE a.Id_Ubicacion = u.Id_Ubicacion
                                                         ORDER BY Fecha DESC")
                                or die ('Fallo en la consulta');

                    $nfilas = mysqli_num_rows($consulta);

                    if($nfilas > 0)
                    {
                        for($i = 0; $i < $nfilas; $i++)
                        {
                            $fila = mysqli_fetch_array($consulta);
                            $fecha = substr($fila['Fecha'], 8).'/'.substr($fila['Fecha'], 5, 2).'/'.substr($fila['Fecha'], 0, 4);

                            echo "  <div class='row'>
                                        <h2 class='page-header'>ID: ".$fila['Id_Adopcion']."</h2>
                                        <div class='col-md-6'>
                                            <div class='thumbnail'>";

                            if($fila['Foto'] == "")
                            {
                                echo        "<img class='img-responsive' src='../img/sinFoto.png' alt='' style='width:400px; height:350px; border:1 solid #122235'>";
                            }

                            else
                            {
                                echo        "<img class='img-responsive' src='../images/$fila[Foto]' alt='' style='width:400px; height:350px; border:1 solid #122235'>";
                            }


                            echo            "<button type='button' class='btn btn-primary' style='Position:Absolute; left:88%; top:83%' name='regresar' id='regresar' value='' onclick='eliminar_adopcion(".$fila['Id_Adopcion'].")'><img src='../img/eliminar.png' style='width:20px; height:20px;'></button> 
                                            </div>
                                        </div>
                                        <div class='col-md-6'>
                                            <h3>Datos de Contacto</h3>
                                            <p><b>Dueño:</b>       ".$fila['Usuario']."</p>
                                            <p><b>Teléfono:</b>        ".$fila['Telefono']."</p>
                                            <p><b>E-mail:</b>       ".$fila['Email']."</p>
                                            <h3>Datos de la Mascota</h3>
                                            <p><b>Nombre:</b>      ".$fila['Mascota']."</p>
                                            <p><b>Tipo:</b>        ".$fila['Tipo']."</p>
                                            <p><b>Raza:</b>        ".$fila['Raza']."</p>
                                            <p><b>Color:</b>       ".$fila['Color']."</p>
                                            <p><b>Género:</b>      ".$fila['Genero']."</p>
                                            <p><b>Descripción:</b>     ".$fila['Descripcion']."</p>
                                            <p><b>Ubicación:</b>     ".$fila['Estado']."</p>
                                            <br>
                                            <p><b>Fecha de publicación:</b> ".$fecha."</p>";

                            if($fila['Status'] == 'Activo')
                            {
                                echo "  <br>
                                        <p><b>Status: <font color='#088A08'>".$fila['Status']."</font></b></p>";
                            }

                            elseif ($fila['Status'] == 'Concluído')
                            {
                                echo "  <br>
                                        <p><b>Status: <font color='#DF7401'>".$fila['Status']."</font></b></p>";
                            }

                            elseif ($fila['Status'] == 'Expirado')
                            {
                                echo "  <br>
                                        <p><b>Status: <font color='#DF0101'>".$fila['Status']."</font></b></p>";
                            }
                            
                            echo "  </div>
                                </div>";
                        }
                    }
                ?>
            </div>
            <hr>

            <!-- Footer -->
            <footer>
                <div class="row">
                    <div class="col-lg-12">
                        <p>&copy; Peluditos 2015</p>
                        <?php
                            cerrar_sesion();
                        ?>
                    </div>
                </div>
            </footer>

        </div>
        <!-- /.container -->

        <!-- jQuery Version 1.11.0 -->
        <script src="js/jquery-1.11.0.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>
